#!/bin/bash

[[ $- != *i* ]] && return

[ -z "$PS1" ] && return

if [ -n "$SSH_CLIENT" ]; then
	export TERM=xterm
fi

# allow ctrl-s and ctrl-q to be used
stty -ixon

[ -f ~/.aliases ] && . ~/.aliases 
[ -f ~/.common ] && . ~/.common

min-prompt() {
    _min_prompt '\w' '\[' '\]'
}

box-prompt() {
    _box_prompt '\w' $(hostname)
}

powerline-prompt() {
    _powerline_prompt '\w' $(hostname) '\[' '\]'
}

greeting-min() {
    _greeting_min 'bash' '\e[33m'
}

greeting-powerline() {
    _greeting_powerline ' \e[34;43m\e[0;43m bash \e[33;49m\e[m'
}

# min-prompt
box-prompt
# powerline-prompt

greeting-min
# greeting-powerline
