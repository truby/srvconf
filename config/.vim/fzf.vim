
Plug 'junegunn/fzf', { 'on': 'Files' }
Plug 'junegunn/fzf.vim', { 'on': 'Files' }

nnoremap <C-p> :Files<CR>
" nnoremap <C-g> :GFiles<CR>

let $FZF_DEFAULT_COMMAND = "fd -t f -H -E .git" 
