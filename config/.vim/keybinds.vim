
inoremap <C-@> <Esc>

" Leader
let mapleader = " "
nnoremap <leader>w :w<CR>
nnoremap <leader>q :q<CR>
nnoremap <leader>z :wq<CR>
nnoremap <leader>n :noh<CR>
nnoremap <leader>r :%s/

" Move lines up and down
nnoremap <leader>j mz:m+<CR>
nnoremap <leader>k mz:m-2<CR>

" Splits
nnoremap <leader>h :sp<CR>
nnoremap <leader>v :vsp<CR>
nnoremap <leader>- :sp<CR>
nnoremap <leader>\ :vsp<CR>

" Spell checking
nnoremap <leader>S :setlocal spell!<CR>

" Makes j and k behave on multilines
nnoremap j gj
nnoremap k gk

" tab follows brackets because % is too hard
nnoremap <tab> %

" Easy navigating between split panes
nnoremap <C-j> <C-W><C-j>
nnoremap <C-k> <C-W><C-k>
nnoremap <C-l> <C-W><C-l>
nnoremap <C-h> <C-W><C-h>

nnoremap <C-e> :tabprevious<CR>
nnoremap <C-r> :tabnext<CR>
nnoremap <silent> <C-x> :execute 'silent! tabmove ' . (tabpagenr()-2)<CR>
nnoremap <silent> <C-c> :execute 'silent! tabmove ' . (tabpagenr()+1)<CR>

" Completion addon agnostic tab / shift tab for menus
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
