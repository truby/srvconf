
Plug 'tpope/vim-fugitive', { 'on': 'Gstatus' }
Plug 'junegunn/gv.vim', { 'on': 'GV' }

nnoremap <leader>gs :Gstatus<CR>
nnoremap <leader>gf :Gfetch<CR>
nnoremap <leader>gu :Gpull<CR>
nnoremap <leader>ga :Git add .<CR>
nnoremap <leader>gc :Gcommit<CR>
nnoremap <leader>gp :Gpush<CR>
nnoremap <leader>gv :GV<CR>

