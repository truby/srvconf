
" Format the status line
set statusline=
" cut at start
" set statusline+=%<

function! ModeFormatted() abort
    let l:mode = mode()
    if l:mode == 'i'
        return ' I '
    elseif l:mode == 'v'
        return ' V '
    else
        return ''
    endif
endfunction

set statusline+=%#User7#%{ModeFormatted()}%#User8#

" flags
set statusline+=%#User9#
set statusline+=%h%m%r%w
set statusline+=%#User6#%*

" relative filename
" set statusline+=%f\  
set statusline+=\ %f\  

" filetype
set statusline+=%y\  

" flags
" set statusline+=%h%m%r%w

" right align
" set statusline+=%=
set statusline+=%#LineNr#%=%#StatusLine#

" line number / lines
set statusline+=\ %l\/%L\ 

" set statusline+=%#User4#%#User7#%{ModeFormatted()}%*

" line number and column
" set statusline+=\ %l,%c\  
" Percentage through file
" set statusline+=%P\  

