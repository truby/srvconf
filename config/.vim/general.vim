syntax enable

" Show line numbers
set number

" Relative line numbers
set number relativenumber

" Replace tabs with 4 spaces
set expandtab
set shiftwidth=4
set tabstop=4

set history=100

filetype plugin on
filetype indent on

" Set 'nocompatible' to ward off unexpected things that your distro might
" have made, as well as sanely reset options when re-sourcing .vimrc
set nocompatible

" The 'hidden' option allows you to re-use the same
" window and switch from an unsaved buffer without saving it first. Also allows
" you to keep an undo history for multiple files when re-using the same window
" in this way. Note that using persistent undo also lets you undo in multiple
" files even in the same window, but is less efficient and is actually designed
" for keeping undo history after closing Vim entirely. Vim will complain if you
" try to quit without saving, and swap files will keep you safe if your computer
" crashes.
set hidden

" turn off backups, since most stuff is in source control
set nobackup
set nowritebackup
set nowb
set noswapfile
set updatetime=100

" set to auto read when a file is changed from the outside
set autoread

" when using %s/ find and replace, the last g is implied, since
" the default behavior is to replace the first occurence
set gdefault

" Display the cursor position on the last line of the screen or in the status
" line of a window
set ruler

" Better command-line completion
set wildmenu

" Better auto completion
set wildmode=longest,list,full

" ignore binary files
set wildignore+=*/tmp/*,*.o,*.so,*.swp,*.zip
 
" Show partial commands in the last line of the screen
set showcmd

" Use case insensitive search, except when using capital letters
set ignorecase
set smartcase
set hlsearch
set incsearch
set lazyredraw

" Allow backspacing over autoindent, line breaks and start of insert action
set backspace=indent,eol,start

" When opening a new line and no filetype-specific indenting is enabled, keep
" the same indent as the line you're currently on. Useful for READMEs, etc.
set autoindent
 
" Stop certain movements from always going to the first character of a line.
" While this behaviour deviates from that of Vi, it does what most users
" coming from other editors would expect.
set nostartofline

" Always show the status line
set laststatus=2

" enable use of mouse for all modes
set mouse=a
if !has('nvim')
    set ttymouse=sgr
endif

" More natural split opening
set splitbelow splitright

" We show this on status line
set noshowmode

set shell=bash

autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

colorscheme truby

