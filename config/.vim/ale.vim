" ALE
if has('nvim')
    Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
    let g:deoplete#enable_at_startup = 1
endif

let g:ale_completion_enabled = 1

Plug 'dense-analysis/ale'

" For the left column to be always open
" let g:ale_sign_column_always = 1
" set omnifunc=ale#completion#OmniFunc
" let g:ale_completion_autoimport = 1

let g:ale_sign_error = '>>'
let g:ale_sign_warning = '--'

nnoremap <leader>[ :ALEPreviousWrap<CR>
nnoremap <leader>] :ALENextWrap<CR>

nnoremap <F8> :ALEFix<CR>

nnoremap <F7> :ALEGoToDefinition<CR>
nnoremap <leader>d :ALEGoToDefinition<CR>

nnoremap <F6> :ALEFindReferences<CR>
nnoremap <leader>f :ALEFindReferences<CR>

" let g:ale_set_loclist = 0
" let g:ale_set_quickfix = 1
" let g:ale_fix_on_save = 1

let g:ale_fixers = { '*': ['remove_trailing_lines', 'trim_whitespace'] }

function! LintCount() abort
    return ale#statusline#Count(bufnr(''))
endfunction

function! LintErrors() abort
    let l:counts = LintCount()
    let l:all_errors = l:counts.error + l:counts.style_error
    return l:all_errors == 0 ? '' : printf("%dE ", all_errors)
endfunction

function! LintWarnings() abort
    let l:counts = LintCount()
    let l:all_non_errors = l:counts.total - l:counts.error - l:counts.style_error
    return l:all_non_errors == 0 ? '' : printf("%dW ", all_non_errors)
endfunction

" set statusline^=%*
" set statusline^=%#User1#%{LintErrors()}
" set statusline^=%#User3#%{LintWarnings()}

set statusline+=%#User3#%{LintWarnings()}
set statusline+=%#User1#%{LintErrors()}
set statusline+=%*
